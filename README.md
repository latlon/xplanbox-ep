# xPlanBox Entwicklungspartnerschaft

Bitte verwenden Sie den Issue-Tracker unter [Issues](../../issues/) um Probleme oder Vorschläge für die Weiterentwicklung der Lösung xPlanBox zu dokumentieren.

Im [Wiki](../../wiki/) finden Sie Informationen zur Lösung xPlanBox und der Entwicklungspartnerschaft. 

Weitere Informationen und den Quellcode der Komponenten der xPlanBox finden Sie auf der Open CoDE-Plattform unter der Adresse [https://gitlab.opencode.de/](https://gitlab.opencode.de/diplanung/ozgxplanung).

# Autoren und Verantwortlich

Hersteller der xPlanBox - lat/lon GmbH, Bonn.

## Kontakt

© 2012-2023 lat/lon gesellschaft für raumbezogene informationssysteme mbH  
Im Ellig 1  
53343 Wachtberg  
Tel: +49 +228 24 333 784    
info@lat-lon.de  
https://www.lat-lon.de  
twitter: https://twitter.com/latlon_de  
GitHub: https://github.com/lat-lon